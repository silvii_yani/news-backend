"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
// *added* import for weather route
const news_1 = __importDefault(require("./news"));
const router = express_1.Router();
// *change here to address routes*
router.use('/news', news_1.default);
exports.default = router;
