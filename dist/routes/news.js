"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = __importStar(require("express"));
const db_config_1 = require("./db_config");
const router = express.Router();
router.get('/', (request, response) => {
    const kategori = request.query.kategori;
    let daftarBerita = [];
    db_config_1.db.connect(() => {
        let sql = `SELECT * FROM berita`;
        if (kategori !== undefined) {
            sql = `SELECT * FROM berita WHERE kategori="${kategori}"`;
        }
        db_config_1.db.query(sql, (err, result) => {
            if (err) {
                throw err;
            }
            daftarBerita = result;
            response.json(daftarBerita);
        });
    });
});
router.get('/:id', (request, response) => {
    const id = request.params.id;
    db_config_1.db.connect(() => {
        const sql = `SELECT * FROM berita WHERE id="${id}"`;
        db_config_1.db.query(sql, (err, result) => {
            if (err) {
                throw err;
            }
            if (result.length === 0) {
                return response.status(404).json({ error: 'Not Found' });
            }
            response.json(result[0]);
        });
    });
});
router.delete('/', (req, res) => {
    res.send('Got a DELETE request at /user');
});
exports.default = router;
