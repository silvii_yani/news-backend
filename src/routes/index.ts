import { Router } from "express";
// *added* import for weather route
import News from "./news";
const router = Router();
// *change here to address routes*
router.use('/news', News);

export default router;
