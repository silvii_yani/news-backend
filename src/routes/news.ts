import * as express from "express";
import { db } from "./db_config";

const router = express.Router();

router.get('/', (request, response) => {

    const kategori = request.query.kategori;
    let daftarBerita: any[] = [];

    db.connect(() => {

        let sql=`SELECT * FROM berita`;
        if (kategori!==undefined) {
            sql = `SELECT * FROM berita WHERE kategori="${kategori}"`;
        }

        db.query(sql, (err: Error, result: any) => {
            if (err) {
                throw err;
            }

            daftarBerita = result;
            response.json(daftarBerita);
        });

    });
});

router.get('/:id', (request,response)=>{

    const id = request.params.id;
    db.connect(() => {

        const sql=`SELECT * FROM berita WHERE id="${id}"`;

        db.query(sql, (err: Error, result: any[]) => {
            if (err) {
                throw err;
            }
            if (result.length === 0){
                return response.status(404).json({error:'Not Found'});
            }
            response.json(result[0]);
        });

    });
});

router.delete('/', (req, res)=>{
    res.send('Got a DELETE request at /user')
  })
export default router;