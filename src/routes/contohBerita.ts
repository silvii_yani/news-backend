const berita1 = {
    judul: "Ridwan Kamil Ingatkan Oded soal Kebakaran 2 Pasar di Bandung",
    kategori: "Lingkungan",
    konten: "Alhamdulillah saya lihat Pak Oded Wali Kota Bandung bisa hadir. Kemarin saya datang ke rumah sakit di hari Lebaran alhamdulilah sudah sehat lagi. Harus sehat Pak Oded karena dibutuhkan rakyat Bandung. Ngan titip pasar tong kahuruan wae (tolong titip papar jangan sampai kebakaran terus). Tolong dicek,' ujar Emil, sapaan akrabnya. Artikel ini telah tayang di Kompas.com dengan judul 'Ridwan Kamil Ingatkan Oded soal Kebakaran 2 Pasar di Bandung'",
}
const berita2 = {
    judul: "Gempa guncang Tasikmalaya",
    kategori: "Cuaca",
    konten: "Diguncang kurang lebih 6 SR",
};

const berita3 = {
    judul: "Banjir Citarum",
    kategori: "Cuaca",
    konten: "Banjir disebabkan oleh sampah"
};
export const contohBerita = [berita1, berita2, berita3];